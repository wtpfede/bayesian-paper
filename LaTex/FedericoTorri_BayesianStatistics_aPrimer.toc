\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {}Introduction}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {0.1}A foreword}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {0.2}What's in this book?}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {0.3}How can I run this book on my computer?}{6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {}Part 1}{7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Introducing Bayes\IeC {\textquoteright } rule}{7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {}Part 2}{9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Bayesian statistics: an analytical approach}{9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}On priors}{10}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}On hypothesis testing:}{13}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {}Part 3}{17}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Bayesian statistics: a computational approach}{17}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}On sampling}{18}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}On monte carlo markov chain approximations}{19}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}On Metropolis-Hastings}{20}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}On multidimensional parameter sampling }{23}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.6}On Gibbs sampling }{24}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {}Part 4}{27}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Bayesian statistics: an applied approach}{27}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}On why we use pymc3}{27}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Categorical models: a case study}{29}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}On sampling quality, convergence and efficiency}{31}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5}On solving convergence issues}{33}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.6}Reparametrizing models: a case study}{35}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.7}Continuous models: a case study}{38}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.8}On model evaluation: Bayes' Factor}{41}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.9}On model evaluation: Information Criteria}{43}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.10}On model evaluation: Leave-One-Out validation}{46}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.11}Choosing between models: a case study}{48}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {}Part 5}{57}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}The future: ADVI and bayesian deep learning}{57}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}On deriving the ADVI algorithm}{57}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Bayesian deep learning: a case study}{60}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}Conclusion: coming full circle}{62}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {}Appendix}{63}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Installing pymc3}{63}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Running pymc3 on the GPU with CUDA}{64}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {}Bibliography}{66}
