For decades now, the statistical field has been divided between the Bayesian school of thinking and the Frequentist school of thinking. Indeed, even as a student, it is almost impossible not to get suckered into one of the two fields, and to very quickly pick a favourite despite having little to no technical background or experience to actually make a meaningful choice.
\jum
In my experience, there’s no ‘winner’ or ‘loser’ in this debate. The two approaches generally lead to similar results (with a few exceptions, see [appendix A]), and while there sometimes is a significantly better tool for the job at hand, usually it’s both insightful and helpful to be able to validate one’s conclusions using different approaches.
\jum
But to tackle this debate, I feel it will be interesting to understand what the main differences are - in other words, how the Frequentists and the Bayesians see the field.
To do so, I will start with an analysis of the debate’s continuum as defined by Andrew Gelman, one of the most prominent living Bayesians. In his seminal 2011 paper, Gelman outlines five different dimensions on which the debate has historically developed:
\begin{center}
\begin{tabular}{|c|c|}
\hline
\textbf{Frequentist}   & \textbf{Bayesian}                                                    \\
\hline
Objective     & Subjective                                                     \\
Procedures    & Models                                                         \\
P-values      & Bayes factors                                                  \\
Deduction     & Induction                                                      \\
Falsification & Pr (model is true) \\ 
 \hline
\end{tabular}
\end{center}
\noindent
\jum
This extremely arbitrary division (that Gelman labels as \say{wrong but influential}), is indeed significant in allowing us to tackle most of the relevant whys of Bayesian statistics.
\jum
The \ter{objective approach} consists in a surefire way of evaluating probabilities: there’s just a single right answer, and its probability is inferred by the frequency in which the outcome of interest appears. In other words, if we denote the number of occurrences $z$ of a given event $A$ over $N$ trials, then we can say that the probability of that event is $P(A)=\lim_{z\to\infty} \frac{z}{N}$ - the classical definition of probability that is taught in most introductory courses.
\jum
As Casella beautifully puts it [refer], the key assumption here is that the underlying true parameters are fixed over the infinitely repeatable set of variable random samples. Given an infinite number of trials, the outcome frequency will converge to a single scalar - its true probability.
\jum
The \ter{subjective approach}, on the other hand, states that there is no such thing as an infinitely repeatable trial with a single underlying parameter. The data will be fixed (as an experiment cannot be repeated in the exact same conditions), but the underlying parameter is variable and subject to a certain degree of uncertainty. To estimate the parameter hence one will need to incorporate external ‘belief’ informations, under the form of prior probabilities.
\jum
To clarify the difference, we can think of the very well known Fisher’s tea lady experiment, in which a woman claims to be able to tell whether milk or tea was added first to a mixed cup. In his 2006 paper [refer], Michael Goldstein suggests a second thought experiment: that of a wizard having paranormal powers, and being able to predict the toss of a coin. 
Both experiments are based on the exact same framework; however, in a subjectivist/bayesian approach, one would probably place a different prior on the probability that the lady can guess milk or tea, compared to that of the wizard having paranormal powers.
\jum
The critic that usually gets leveled by frequentists to bayesians is indeed that such priors based on personal beliefs should have no place in scientific analysis: in other words, there’s no reason why the same analysis on the same data should yield different results depending on the researcher’s beliefs.
\jum
This complaint however is not as valid as it might look. First of all, when leaving the realm of thinking problems and going into the real world, even the frequentist researcher would be confronted with increased scrutiny if unable to reject the cheating hypothesis for the wizard as compared to the tea lady. Since \say{extraordinary claims require extraordinary evidence}, the frequentist researcher would have to present an even more solid framework of thought (such as a larger sample, more experimental repeats, or lower p-values) in order to be believed. As such, the external beliefs would still influence his or her research, but without the rigorous formalization that characterizes the bayesian ‘prior’ approach.
\jum
Additionally, as Gelman correctly points out in his paper on subjectivity [refer], \say{the frequentist mindset locates probabilities in the observer-independent world, so they are in this sense objective. This objectivity, however, is model-based, as an infinite amount of actual replicates cannot exist}. in other words, the assumption of infinite replicability with fixed parameters does not hold in the real world - and so, even though the observer is objective, the model chosen to represent the unattainable infinite replicability is still, at best, subjective.
\jum
The last remark is a practical one: the difference between objective and subjective analysis has became more and more blurred as the frequentist side has started embracing Gelman’s model-based view and adopting derivations of bayesian constructs such as information criteria or lasso shrinkage. And the bayesian side has accepted most of the subjectivity critics and tried to mitigate them by either choosing objective priors that do not require any tuning, are invariant to the chosen parametrization, or are weakly informative.
\jum
The difference between the two fields on the \ter{procedures vs models} point is even more blurred: the choice of objective priors or model iteration that bayesians practice definitely falls under the definition of ‘procedures’, and of course the frequentist approach consists on modelling the data as well.
\jum
What Gelman might be referring to here is rather the higher focus (due to centuries of research and adopted practiques) that frequentists put on statistical tests. Richard McElreath [refer] calls these ‘golems’, after the mythical figures of jewish mythology. 
\say{Instead of a single method for building, refining, and critiquing statistical models, students are offered a zoo of pre-constructed golems known as tests}, writes McElreath, warning users of the dangers of a misunderstood golem in the wild. 
\jum
However, in my opinion this has more to do with the way introductory statistics is thought than with the frequentist school of thought, and indeed, as bayesian statistics becomes more and more tractable thanks to numerical brute-forcing, I’m sure the Bayesian field will have a fair share of ‘golems’ to tame as well.
\jum
The last three items refer to a number of related issues, and can be clustered together for brevity’s sake. The core concept of this last division is basically reflected in the interest (or absence thereof) that Bayesian and Frequentists have in testing whether an hypothesis is falsifiable or not.
\jum
Gelman captures the key differences on these three dimensions beautifully [refer], by writing \say{The Bayes factor approach abandons the Popperian idea of hypothesis testing entirely. [...] Instead you compute the relative posterior probabilities of competing models, with the progress of science corresponding to formerly high-probability models being abandoned in favor of new models that are more supported by the data}. 
\jum
Hence, no hypothesis can be appropriately rejected on a binary scale: there’s no $h_0$ and $h_1$, but rather an evolving continuum of probability ratios. There is no absolute deduction: as Gelman says, \say{A model can be rejected, never accepted}, and there’s no point in rejecting an hypothesis to deduce the correct one, as the space is way too large. Indeed, the bayesian view (with its many schools and nuances) boils down to inductively reason about the model that best approximates the \ter{real, data-generating model}, and not the data itself.
\jum
Indeed, in the Bayesian school there is no absolute ‘eureka’ moment, but rather a continuous updating of one’s beliefs and convinctions. And this of course leads us to the last point: it is impossible to falsify a statement as the frequentist school would like: for bayesians, the model’s performance related to the constant stream of data and beliefs can be expressed as a probability. And so no model will be ‘true’ or ‘false’, but rather more or less believable.
\jum
I believe this latter approach better approximate the way humans think. In fact, I have found the 